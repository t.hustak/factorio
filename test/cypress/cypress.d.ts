declare namespace Cypress {
    interface Chainable<Subject> {
        tickGame(): Chainable<any>

        stoneDeposit(): Chainable<any>

        findFirstResource(resource): Chainable<any>

        findLastResource(resource): Chainable<any>

        buildMine(): Chainable<any>

        mineResource(resource): Chainable<any>

        buildFactory(resource): Chainable<any>

        destroyBuilding(): Chainable<any>

        restartApp(): Chainable<any>

        getWorld(): Chainable<any>
    }
}