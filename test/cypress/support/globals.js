export var ResourceType = {
    NONE: "None",

    STONE: "Stone",
    COAL: "Coal",
    COPPER_ORE: "CopperOre",
    IRON_ORE: "IronOre",
    OIL: "Oil",

    IRON_PLATE: "IronPlate",
    STEEL: "Steel",
    STEEL_CASING: "SteelCasing",
    ROCKET_SHELL: "RocketShell",
    COPPER_PLATE: "CopperPlate",
    WIRE: "Wire",
    CIRCUIT: "Circuit",
    COMPUTER: "Computer",
    PETROLEUM: "Petroleum",
    BASIC_FUEL: "BasicFuel",
    SOLID_FUEL: "SolidFuel",
    ROCKET_FUEL: "RocketFuel"
}