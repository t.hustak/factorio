// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('startGame', (playerName, options = {}) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', (playerName, options = {}) => {
    cy.wait(1000); // game ticks like that
});

Cypress.Commands.add('stoneDeposit', (options = {}) => {
    return cy.get('#bank-Stone-amount').invoke('text');
});

Cypress.Commands.add('findFirstResource', (resource, options = {}) => {
    return cy.get('td > div > img[alt="Icon of ' + resource + '"]:first');
});

Cypress.Commands.add('findLastResource', (resource, options = {}) => {
    return cy.get('td > div > img[alt="Icon of ' + resource + '"]:last');
});

Cypress.Commands.add('mineResource', (options = {}) => {
    cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildMine', (options = {}) => {
    cy.get('#action-BuildMine').click();
});

Cypress.Commands.add('destroyBuilding', (options = {}) => {
    cy.get('#action-DestroyBuilding').click();
});

Cypress.Commands.add('buildFactory', (resource, options = {}) => {
    cy.get('#action-Build' + resource + 'Factory').click();
});

// -- API methods --

Cypress.Commands.add('restartApp', () => {
    cy.request('POST','http://localhost:8080/restart')
});

Cypress.Commands.add('getWorld', () => {
    return cy.request('GET','http://localhost:8080/world').its('body')
});

