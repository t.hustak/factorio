/// <reference types="Cypress" />

import {ResourceType} from "../../support/globals";

describe('Failover (or sometimes called negative) scenarios', () => {

    it('Non-existing coordinates', () => {
        cy.request({
            method: 'POST',
            url: 'http://localhost:8080/turns',
            body: {PlayerName: "Professor", action: "BuildIronPlateFactory", coordinates: {X: 10000, Y: 10000}},
            failOnStatusCode: false
        }).then((response) => {
            expect(response.status).to.eq(400)
        })
    })

    it('Non-existing resource', () => {
        cy.startGame('Moscow')
        cy.findFirstResource(ResourceType.OIL).parent().invoke('attr', 'id')
            .then((id) => {
                    var coordinates = id.split(/-/g).slice(1)
                    cy.request({
                        method: 'POST',
                        url: 'http://localhost:8080/turns',
                        body: {PlayerName: "Rio", action: "MineResource", coordinates: {X: parseInt(coordinates[0]), Y: parseInt(coordinates[1])}},
                        failOnStatusCode: false
                    }).then((response) => {
                        expect(response.status).to.eq(200)
                    })
                }
            )
    })

    it('Non-existing user', () => {
        cy.request({
            method: 'POST',
            url: 'http://localhost:8080/turns',
            body: {PlayerName: "Tom", action: "BuildIronPlateFactory", coordinates: {X: 1, Y: 1}},
            failOnStatusCode: false
        }).then((response) => {
            expect(response.status).to.eq(400)
        })
    })
})