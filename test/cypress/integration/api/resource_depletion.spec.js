/// <reference types="Cypress" />

import {ResourceType} from '../../support/globals.js';

describe('Resources are not infinite', () => {

    before(() => {
        cy.restartApp()
        cy.startGame('Helsinki')
    });

    it('Mine resource and then check the source', () => {
        cy.findFirstResource(ResourceType.IRON_ORE).parent().invoke('attr', 'id')
            .then((id) => {
                var coordinates = id.split(/-/g).slice(1)
                cy.log(id)
                cy.log(coordinates[0])
                cy.log(coordinates[1])

                cy.request('GET', 'http://localhost:8080/world').its('body').then((body) => {
                    var parsed = JSON.parse(body)
                    var initialAmount = parsed.tiles[parseInt(coordinates[1])][parseInt(coordinates[0])].resourceSite.Amount
                    mineResource(coordinates)
                    cy.wait(2000)
                    cy.request('GET', 'http://localhost:8080/world').its('body').then((body) => {
                            var parsed = JSON.parse(body)
                            var currentAmount = parsed.tiles[parseInt(coordinates[1])][parseInt(coordinates[0])].resourceSite.Amount
                            expect(currentAmount).to.eq(initialAmount - 1)
                        }
                    )
                })
            })
    })

    function mineResource(coordinates) {
        cy.request('POST', 'http://localhost:8080/turns', {
            PlayerName: "Helsinki",
            action: "MineResource",
            coordinates: {X: parseInt(coordinates[0]), Y: parseInt(coordinates[1])}
        })
    }
});