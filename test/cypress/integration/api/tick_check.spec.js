/// <reference types="Cypress" />

import {ResourceType} from '../../support/globals.js';

describe('Checking the ticks of the server', () => {

    before(() => {
        cy.restartApp()
        cy.startGame('Moscow')
    });

    it('Basic tick test', () => {
        cy.findFirstResource(ResourceType.IRON_ORE).parent().invoke('attr', 'id')
            .then((id) => {
                    var coordinates = id.split(/-/g).slice(1)
                    cy.log(id)
                    cy.log(coordinates[0])
                    cy.log(coordinates[1])

                    for (let i = 0; i < 20; i++) {
                        cy.request('POST', 'http://localhost:8080/turns', {
                            PlayerName: "Moscow",
                            action: "MineResource",
                            coordinates: {X: parseInt(coordinates[0]), Y: parseInt(coordinates[1])}
                        })
                    }

                    cy.request('GET', 'http://localhost:8080/bank').its('body').then((body) => {
                        var parsed = JSON.parse(body)
                        var ironOreAmount = getResourceAmount(parsed, "IronOre")
                        expect(ironOreAmount).to.be.lessThan(10)
                    })
                    cy.wait(10000)

                    cy.request('GET', 'http://localhost:8080/bank').its('body').then((body) => {
                        var parsed = JSON.parse(body)
                        var ironOreAmount = getResourceAmount(parsed, "IronOre")
                        expect(ironOreAmount).to.be.lessThan(20)
                    })

                    cy.wait(10000)

                    cy.request('GET', 'http://localhost:8080/bank').its('body').then((body) => {
                        var parsed = JSON.parse(body)
                        var ironOreAmount = getResourceAmount(parsed, "IronOre")
                        expect(ironOreAmount).to.equal(20)
                    })
                }
            )

    })

    function getResourceAmount(bank, resource) {
        for (let i = 0; i < bank.length; i++) {
            if (bank[i].Resource == resource) {
                return bank[i].Amount
            }
        }
    }
})