/// <reference types="Cypress" />

import {ResourceType} from '../../support/globals.js';

context('Top menu icons', () => {

    before(() => {
        cy.startGame('Professor')
    });

    it('Correct icons', function () {
        checkThatNumberOfResourcesIs(17)

        checkTopIconFor(ResourceType.STONE)
        checkTopIconFor(ResourceType.COPPER_ORE)
        checkTopIconFor(ResourceType.IRON_ORE)
        checkTopIconFor(ResourceType.OIL)
        checkTopIconFor(ResourceType.IRON_PLATE)
        checkTopIconFor(ResourceType.STEEL)
        checkTopIconFor(ResourceType.STEEL_CASING)

        checkTopIconFor(ResourceType.ROCKET_SHELL)
        checkTopIconFor(ResourceType.COPPER_PLATE)
        checkTopIconFor(ResourceType.WIRE)
        checkTopIconFor(ResourceType.CIRCUIT)
        checkTopIconFor(ResourceType.COMPUTER)
        checkTopIconFor(ResourceType.PETROLEUM)
        checkTopIconFor(ResourceType.BASIC_FUEL)
        checkTopIconFor(ResourceType.SOLID_FUEL)
        checkTopIconFor(ResourceType.ROCKET_FUEL)
    })

    function checkTopIconFor(resource) {
        cy.get('img[alt="' + resource + ' Icon"]:first')
            .should('have.attr', 'src')
            .then((src) => {
                expect(src).to.equal("img/" + resource + ".png")
            })
    }

    function checkThatNumberOfResourcesIs(numberOfResources) {
        cy.get('div.bank').find('img').should('have.length', numberOfResources)
    }
});