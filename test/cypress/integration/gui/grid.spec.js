/// <reference types="Cypress" />

context('The world', () => {
    beforeEach(() => {
        cy.startGame('Nairobi');
    });

    it('Check the grid size is correct', function () {
        cy.get('table').find('tr').should('have.length', 30)
        cy.get('tr:first').find('td').should('have.length', 50)
        cy.get('table').find('td').should('have.length', 1500)
    })
});