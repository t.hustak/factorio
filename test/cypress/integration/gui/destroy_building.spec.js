/// <reference types="Cypress" />

import {ResourceType} from "../../support/globals";

context('Destroy buildings', () => {

    before(() => {
        cy.restartApp()
        cy.startGame('Oslo')
    });

    it('Check that a mine can be destroyed', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstResource(ResourceType.STONE).click()
            cy.mineResource()
        }
        cy.wait(5000)

        cy.findFirstResource(ResourceType.STONE).click()
        cy.buildMine()
        cy.wait(2000)

        cy.findFirstResource(ResourceType.STONE + "Mine").click()
        cy.destroyBuilding()
        cy.wait(2000)
        cy.findFirstResource(ResourceType.STONE + "Mine").should('not.exist')
    })

    it('Check that a factory can be destroyed', function () {
        cy.findFirstResource(ResourceType.COPPER_ORE).click()
        cy.buildFactory(ResourceType.COPPER_PLATE)
        cy.wait(2000)

        cy.findFirstResource(ResourceType.COPPER_PLATE + "Factory").click()
        cy.destroyBuilding()
        cy.wait(2000)
        cy.findFirstResource(ResourceType.COPPER_PLATE + "Factory").should('not.exist')
    })
});