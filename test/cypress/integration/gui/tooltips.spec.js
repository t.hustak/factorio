/// <reference types="Cypress" />

import {ResourceType} from '../../support/globals.js';

context('Tooltips', () => {

    before(() => {
        cy.restartApp()
        cy.startGame('Rio')
    });

    it('Empty tile', function () {
        tileContainsSimpleTooltip({Resource: ResourceType.NONE, Tooltip: "Nothing"})
    })

    it('Remaining resources', function () {
        tileContainsTooltipWithRemainingResources({Resource: ResourceType.COPPER_ORE, Tooltip: "of Copper Ore"})
        tileContainsTooltipWithRemainingResources({Resource: ResourceType.IRON_ORE, Tooltip: "of Iron Ore"})
    })

    it('Mines', function () {
        enableForBuildingMines()
        buildMine(ResourceType.STONE)
        tileContainsTooltipWithMine({Resource: ResourceType.STONE, Tooltip: "Stone Mine ("})
        buildMine(ResourceType.COPPER_ORE)
        tileContainsTooltipWithMine({Resource: ResourceType.COPPER_ORE, Tooltip: "Copper Ore Mine ("})
        buildMine(ResourceType.IRON_ORE)
        tileContainsTooltipWithMine({Resource: ResourceType.IRON_ORE, Tooltip: "Iron Ore Mine ("})
        buildMine(ResourceType.OIL)
        tileContainsTooltipWithMine({Resource: ResourceType.OIL, Tooltip: "Oil Mine ("})

    })

    it('Factories', function () {
        buildFactory({Factory: ResourceType.IRON_PLATE, Tile: ResourceType.NONE})
        tileContainsTooltipWithFactory({Resource: ResourceType.IRON_PLATE, Tooltip: "Iron Plate Factory"})

        buildFactory({Factory: ResourceType.STEEL, Tile: ResourceType.COPPER_ORE})
        tileContainsTooltipWithFactory({Resource: ResourceType.STEEL, Tooltip: "Steel Factory"})

        buildFactory({Factory: ResourceType.STEEL_CASING, Tile: ResourceType.STONE})
        tileContainsTooltipWithFactory({Resource: ResourceType.STEEL_CASING, Tooltip: "Steel Casing Factory"})

        buildFactory({Factory: ResourceType.ROCKET_SHELL, Tile: ResourceType.IRON_ORE})
        tileContainsTooltipWithFactory({Resource: ResourceType.ROCKET_SHELL, Tooltip: "Rocket Shell Factory"})

        buildFactory({Factory: ResourceType.COPPER_PLATE, Tile: ResourceType.OIL})
        tileContainsTooltipWithFactory({Resource: ResourceType.COPPER_PLATE, Tooltip: "Copper Plate Factory"})

        buildFactory({Factory: ResourceType.WIRE, Tile: ResourceType.IRON_ORE})
        tileContainsTooltipWithFactory({Resource: ResourceType.WIRE, Tooltip: "Wire Factory"})

        buildFactory({Factory: ResourceType.CIRCUIT, Tile: ResourceType.COPPER_ORE})
        tileContainsTooltipWithFactory({Resource: ResourceType.CIRCUIT, Tooltip: "Circuit Factory"})

        buildFactory({Factory: ResourceType.COMPUTER, Tile: ResourceType.STONE})
        tileContainsTooltipWithFactory({Resource: ResourceType.COMPUTER, Tooltip: "Computer Factory"})

        buildFactory({Factory: ResourceType.PETROLEUM, Tile: ResourceType.NONE})
        tileContainsTooltipWithFactory({Resource: ResourceType.PETROLEUM, Tooltip: "Petroleum Factory"})

        buildFactory({Factory: ResourceType.BASIC_FUEL, Tile: ResourceType.COPPER_ORE})
        tileContainsTooltipWithFactory({Resource: ResourceType.BASIC_FUEL, Tooltip: "Basic Fuel Factory"})

        buildFactory({Factory: ResourceType.SOLID_FUEL, Tile: ResourceType.NONE})
        tileContainsTooltipWithFactory({Resource: ResourceType.SOLID_FUEL, Tooltip: "Solid Fuel Factory"})

        buildFactory({Factory: ResourceType.ROCKET_FUEL, Tile: ResourceType.OIL})
        tileContainsTooltipWithFactory({Resource: ResourceType.ROCKET_FUEL, Tooltip: "Rocket Fuel Factory"})
    })

    function tileContainsSimpleTooltip(tile) {
        cy.findFirstResource(tile.Resource).trigger('mouseover')
        cy.get('span.tooltip').invoke('text').should('eq', tile.Tooltip)
        cy.findFirstResource(tile.Resource).trigger('mouseout')
    }

    function tileContainsTooltipWithRemainingResources(tile) {
        cy.findFirstResource(tile.Resource).trigger('mouseover')
        cy.get('span.tooltip').invoke('text')
            .then((text) => {
                var fullText = text;
                var pattern = /[0-9]+/g;
                var number = fullText.match(pattern);
                expect(number).to.not.be.null
                expect(text).to.contain(tile.Tooltip)
            })
        cy.findFirstResource(tile.Resource).trigger('mouseout')
    }

    function tileContainsTooltipWithFactory(tile) {
        cy.findFirstResource(tile.Resource + "Factory").trigger('mouseover')
        cy.get('span.tooltip').invoke('text').should('eq', tile.Tooltip)
        cy.findFirstResource(tile.Resource + "Factory").trigger('mouseout')
    }

    function tileContainsTooltipWithMine(tile) {
        cy.findFirstResource(tile.Resource + "Mine").trigger('mouseover')
        cy.get('span.tooltip').invoke('text').should('contain', tile.Tooltip)
        cy.findFirstResource(tile.Resource + "Mine").trigger('mouseout')
    }

    function enableForBuildingMines() {
        for (let i = 0; i < 5; i++) {
            cy.findFirstResource(ResourceType.STONE).click();
            cy.mineResource();
        }
        cy.wait(5000)
    }

    function buildMine(resource) {
        cy.findFirstResource(resource).click();
        cy.buildMine()
        cy.wait(2000)
    }

    function buildFactory(resource) {
        cy.findFirstResource(resource.Tile).click();
        cy.buildFactory(resource.Factory)
        cy.wait(2000)
    }
});