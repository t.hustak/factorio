/// <reference types="Cypress" />

import {ResourceType} from '../../support/globals.js';

context('Error messages', () => {

    before(() => {
        cy.restartApp()
        cy.startGame('Berlin')
    });

    it('At least 5 stones are needed to build a mine', function () {
        tryToBuildMineFor(ResourceType.STONE)
        tryToBuildMineFor(ResourceType.COPPER_ORE)
        tryToBuildMineFor(ResourceType.IRON_ORE)
        tryToBuildMineFor(ResourceType.OIL)
    })

    it('At least 5 stones are needed to build a factory', function () {
        tryToBuildFactoryForTile({Factory: ResourceType.IRON_PLATE, Tile: ResourceType.STONE})
        tryToBuildFactoryForTile({Factory: ResourceType.STEEL, Tile: ResourceType.COPPER_ORE})
        tryToBuildFactoryForTile({Factory: ResourceType.STEEL_CASING, Tile: ResourceType.STONE})
        tryToBuildFactoryForTile({Factory: ResourceType.ROCKET_SHELL, Tile: ResourceType.IRON_ORE})
        tryToBuildFactoryForTile({Factory: ResourceType.COPPER_PLATE, Tile: ResourceType.OIL})
        tryToBuildFactoryForTile({Factory: ResourceType.WIRE, Tile: ResourceType.IRON_ORE})
        tryToBuildFactoryForTile({Factory: ResourceType.CIRCUIT, Tile: ResourceType.COPPER_ORE})
        tryToBuildFactoryForTile({Factory: ResourceType.COMPUTER, Tile: ResourceType.STONE})
        tryToBuildFactoryForTile({Factory: ResourceType.PETROLEUM, Tile: ResourceType.NONE})
        tryToBuildFactoryForTile({Factory: ResourceType.BASIC_FUEL, Tile: ResourceType.COPPER_ORE})
        tryToBuildFactoryForTile({Factory: ResourceType.SOLID_FUEL, Tile: ResourceType.NONE})
        tryToBuildFactoryForTile({Factory: ResourceType.ROCKET_FUEL, Tile: ResourceType.OIL})
    })


    function tryToBuildMineFor(resource) {
        cy.findFirstResource(resource).click();

        var alerted = false;
        cy.on('window:alert', msg => alerted = msg);

        cy.buildMine()
            .then(() => expect(alerted).to.match(/You need 5 stone to build something!/)); // if false then alert dialog isn't displayed
    }

    function tryToBuildFactoryForTile(resource) {
        cy.findFirstResource(resource.Tile).click();

        var alerted = false;
        cy.on('window:alert', msg => alerted = msg);

        cy.buildFactory(resource.Factory)
            .then(() => expect(alerted).to.match(/You need 5 stone to build something!/)); // if false then alert dialog isn't displayed
    }
});