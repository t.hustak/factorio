/// <reference types="Cypress" />


context('Available users', () => {

    it('Check that 9 users are available', function () {
        cy.visit('/');

        cy.get('select').find('option').should('have.length', 9)

        cy.get('select').select('Nairobi').should('have.value', 'Nairobi')
        cy.get('select').select('Tokyo').should('have.value', 'Tokyo')
        cy.get('select').select('Berlin').should('have.value', 'Berlin')
        cy.get('select').select('Denver').should('have.value', 'Denver')
        cy.get('select').select('Helsinki').should('have.value', 'Helsinki')
        cy.get('select').select('Rio').should('have.value', 'Rio')
        cy.get('select').select('Moscow').should('have.value', 'Moscow')
        cy.get('select').select('Oslo').should('have.value', 'Oslo')
        cy.get('select').select('Professor').should('have.value', 'Professor')
    })
});