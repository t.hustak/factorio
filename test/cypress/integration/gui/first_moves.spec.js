/// <reference types="Cypress" />

import {ResourceType} from '../../support/globals.js';

context('Mining by hand', () => {
    beforeEach(() => {
        cy.startGame('Denver');
    });

    it('Init Stone Production!', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstResource(ResourceType.STONE).click();
            cy.mineResource();
        }
        cy.findFirstResource(ResourceType.STONE).click();
        cy.buildMine();

        cy.stoneDeposit().should('be.above', 2);
    })
});